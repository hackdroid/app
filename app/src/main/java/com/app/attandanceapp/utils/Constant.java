package com.app.attandanceapp.utils;

public class Constant {
    public static final String BASE_URL = "http://65.0.81.132:7770/";
    public static boolean isConnectedtoWIFI = false ;
    public static final String ACCESS_TOKEN = "access_token";
    public static final String LOGIN_API = "login_api";
    public static final String FORGOT_PASSWORD_API = "forgot_pass_api";
    public static final String OTP_VERIFY_API = "otp_verify_api";
    public static final String CHANGE_PASSWORD_API = "change_password_api";
    public static final String ACCESS_TOKEN_API = "access_token_api";

    public static final String USER_EMAIL = "user_email";
    public static final String USER_NAME = "user_name";
    public static final String RAVINDER_EMAIL = "ravinder.godsvogn@gmail.com";
    public static final String PREETI_EMAIL = "preeti.godsvogn@gmail.com";
    public static final String KHALID_EMAIL = "test@hr.com";

    public static final String CHECK_CONNECTION = "Please check your internet connection.";
    public static final String POST_URL_FILE_UPLOAD_API = "file_upload";
    public static final String POST_URL_SELFIE_UPLOAD_API = "selfie_upload";
    public static final String CHECK_SESSION = "check_session";
}
