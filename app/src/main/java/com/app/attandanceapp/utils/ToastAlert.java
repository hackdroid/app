package com.app.attandanceapp.utils;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.attandanceapp.R;

public class ToastAlert {
    Activity activity;

    public ToastAlert(Activity activity) {
        this.activity = activity;
    }

    /**
     * @param msg Error Message
     */
    public void showError(String msg) {
//        if (msg)
        View view = LayoutInflater.from(activity.getApplicationContext()).inflate(R.layout.toast_error, null, false);
        ImageView imageView = view.findViewById(R.id.image);
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.pulse);
        imageView.setAnimation(animation);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(msg);
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setView(view);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public void showWarning(String msg) {
//        if (msg)
        View view = LayoutInflater.from(activity.getApplicationContext()).inflate(R.layout.toast_warning, null, false);
        ImageView imageView = view.findViewById(R.id.image);
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.pulse);
        imageView.setAnimation(animation);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(msg);
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setView(view);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public void showNoInternet(String msg) {
//        if (msg)
        View view = LayoutInflater.from(activity.getApplicationContext()).inflate(R.layout.toast_no_internet, null, false);
        ImageView imageView = view.findViewById(R.id.image);
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.pulse);
        imageView.setAnimation(animation);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(msg);
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setView(view);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
