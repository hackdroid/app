package com.app.attandanceapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;

import com.app.attandanceapp.R;

public class WifiBroadcastReceiver extends BroadcastReceiver {
    String TAG = WifiBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(action)) {
            SupplicantState supplicantState = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
            if (SupplicantState.isValidState(supplicantState) && supplicantState == SupplicantState.COMPLETED) {
                boolean connected = checkConnectedForWifiDevice(context);
                Log.d(TAG, "onReceive: " + connected);
                Constant.isConnectedtoWIFI = connected;
                Toast.makeText(context  , "Connected to wifi" , Toast.LENGTH_SHORT).show();
            } else {
               Toast.makeText(context  , "Not Connected to wifi" , Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onReceive: Not connected to WIFI");
                Constant.isConnectedtoWIFI = false;
            }
        }else {
            Toast.makeText(context  , "Main if block not working" , Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkConnectedForWifiDevice(Context context) {
        boolean connected = false;
        String desiredMAC = context.getString(R.string.mac_address);
        WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        if (info != null) {
            String bssid = info.getBSSID();
            Log.d(TAG, "checkConnectedForWifiDevice: " + bssid);
            Log.d(TAG, "checkConnectedForWifiDevice: " + bssid);

//            Log.d(TAG, "checkConnectedForWifiDevice: " + IP);
           try {
               connected = bssid.equals(desiredMAC);
           }catch (Exception e){
               e.printStackTrace();
           }
        } else {
            Log.d(TAG, "checkConnectedForWifiDevice: Not connected to wifi");
        }
        return connected;
    }
}
