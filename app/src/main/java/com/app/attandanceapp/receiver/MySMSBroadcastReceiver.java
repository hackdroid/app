package com.app.attandanceapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySMSBroadcastReceiver extends BroadcastReceiver {
    private OTPListener otpReceiver;

    public void initOTPListener(@Nullable OTPListener receiver){
        this.otpReceiver = receiver;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent . getExtras ();
            Status status =(Status) extras . get (SmsRetriever.EXTRA_STATUS);
            OTPListener otpListener;
            switch(status.getStatusCode()) {
                case CommonStatusCodes. SUCCESS :
                    // Get SMS message contents
                    String message =(String) extras . get (SmsRetriever.EXTRA_SMS_MESSAGE);

                    Pattern pattern = Pattern.compile("\\d{6}");
                    Matcher matcher = pattern.matcher((CharSequence)message);

                    if (matcher.find()) {
                        otpListener = this.otpReceiver;
                        if (this.otpReceiver != null) {
                            String otp = matcher.group(0);
                            //            Intrinsics.checkExpressionValueIsNotNull(otp, "matcher.group(0)");
                            otpListener.onOTPReceived(otp);
                        }

                        return;
                    }
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server for SMS authenticity.
                    break;
                case CommonStatusCodes . TIMEOUT :
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    otpListener = this.otpReceiver;
                    if (this.otpReceiver != null) {
                        otpListener.onOTPTimeOut();
                    }
                    break;

            }

        }
    }

    public interface OTPListener{
        void onOTPReceived(String var1);

        void onOTPTimeOut();
    }

}