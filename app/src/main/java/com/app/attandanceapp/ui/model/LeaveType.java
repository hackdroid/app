package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaveType {
    @SerializedName("leave_type_id")
    @Expose
    private int leaveTypeId;
    @SerializedName("leave_type_title")
    @Expose
    private String leaveTypeTitle;
    @SerializedName("leave_type_allowed")
    @Expose
    private int leaveTypeAllowed;
    @SerializedName("leave_type_status")
    @Expose
    private int leaveTypeStatus;

    public int getLeaveTypeId() {
        return leaveTypeId;
    }

    public void setLeaveTypeId(int leaveTypeId) {
        this.leaveTypeId = leaveTypeId;
    }

    public String getLeaveTypeTitle() {
        return leaveTypeTitle;
    }

    public void setLeaveTypeTitle(String leaveTypeTitle) {
        this.leaveTypeTitle = leaveTypeTitle;
    }

    public int getLeaveTypeAllowed() {
        return leaveTypeAllowed;
    }

    public void setLeaveTypeAllowed(int leaveTypeAllowed) {
        this.leaveTypeAllowed = leaveTypeAllowed;
    }

    public int getLeaveTypeStatus() {
        return leaveTypeStatus;
    }

    public void setLeaveTypeStatus(int leaveTypeStatus) {
        this.leaveTypeStatus = leaveTypeStatus;
    }
}
