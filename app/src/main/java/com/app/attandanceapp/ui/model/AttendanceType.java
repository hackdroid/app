package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceType {
    @SerializedName("attandance_type_id")
    @Expose
    private Integer attandanceTypeId;
    @SerializedName("attandance_type_name")
    @Expose
    private String attandanceTypeName;

    public Integer getAttandanceTypeId() {
        return attandanceTypeId;
    }

    public void setAttandanceTypeId(Integer attandanceTypeId) {
        this.attandanceTypeId = attandanceTypeId;
    }

    public String getAttandanceTypeName() {
        return attandanceTypeName;
    }

    public void setAttandanceTypeName(String attandanceTypeName) {
        this.attandanceTypeName = attandanceTypeName;
    }
}
