package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeModel {
    @SerializedName("user_shift_id")
    @Expose
    private Integer userShiftId;
    @SerializedName("user_shift_start")
    @Expose
    private String userShiftStart;
    @SerializedName("user_shift_end")
    @Expose
    private String userShiftEnd;
    @SerializedName("user_shift_ref")
    @Expose
    private Integer userShiftRef;

    public Integer getUserShiftId() {
        return userShiftId;
    }

    public void setUserShiftId(Integer userShiftId) {
        this.userShiftId = userShiftId;
    }

    public String getUserShiftStart() {
        return userShiftStart;
    }

    public void setUserShiftStart(String userShiftStart) {
        this.userShiftStart = userShiftStart;
    }

    public String getUserShiftEnd() {
        return userShiftEnd;
    }

    public void setUserShiftEnd(String userShiftEnd) {
        this.userShiftEnd = userShiftEnd;
    }

    public Integer getUserShiftRef() {
        return userShiftRef;
    }

    public void setUserShiftRef(Integer userShiftRef) {
        this.userShiftRef = userShiftRef;
    }
}
