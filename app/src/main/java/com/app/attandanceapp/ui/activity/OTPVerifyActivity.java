package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.helper.SavedPrefManager;
import com.app.attandanceapp.receiver.MySMSBroadcastReceiver;
import com.app.attandanceapp.ui.model.ForgotPasswordModel;
import com.app.attandanceapp.ui.model.OTPVerifyModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.PinEntryEditText;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

public class OTPVerifyActivity extends AppCompatActivity implements View.OnClickListener {
    String mEmail;
    Context context;
    CountDownTimer countDownTimer;
    PinEntryEditText otpView;
    TextView txtResend, txtTimer, txtMobile;
    String code;
    AVLoadingIndicatorView indicatorView;
    MySMSBroadcastReceiver smsBroadcastReceiver;
    RelativeLayout relativeLayoutVerify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);
        context = this;

        mEmail = getIntent().getStringExtra("email");
        smsBroadcastReceiver = new MySMSBroadcastReceiver();

        initViews();
        retrivedOTPFromDevice();
    }

    private void initViews() {
        indicatorView = findViewById(R.id.avi);
        indicatorView.smoothToHide();

        relativeLayoutVerify = findViewById(R.id.rl_verify_otp);
        otpView = findViewById(R.id.edit_otp);
        txtResend = findViewById(R.id.txt_resend);
        txtTimer = findViewById(R.id.txt_timer);
        txtMobile = findViewById(R.id.txt_mobile);

        txtMobile.setText(mEmail);

        txtResend.setVisibility ( View.GONE );
        txtTimer.setVisibility ( View.VISIBLE );
        disablingResendButton();

        relativeLayoutVerify.setOnClickListener(this);
        txtResend.setOnClickListener(this);

        if (countDownTimer != null){
            countDownTimer.cancel ();
            otpView.setText(otpView.getText().toString());
            otpView.setCursorVisible(false);
            txtResend.setVisibility ( View.GONE );
        }

        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 6) {
                    AppUtils.hide_keyboard(OTPVerifyActivity.this);
                    indicatorView.smoothToShow();
                    otpVerifyApi();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}

        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_verify_otp:
                if (validate()){
                    indicatorView.smoothToShow();
                    otpVerifyApi();
                }
                break;

            case R.id.txt_resend:
                indicatorView.smoothToShow();
                resendOTPMethod();
                break;
        }
    }

    private void retrivedOTPFromDevice(){
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        Task retriever = client.startSmsRetriever();
        retriever.addOnSuccessListener(o -> {
            txtResend.setVisibility ( View.GONE );
            disablingResendButton();
            MySMSBroadcastReceiver.OTPListener otpListener = new MySMSBroadcastReceiver.OTPListener() {
                @Override
                public void onOTPReceived(String otp) {
                    if (countDownTimer != null){
                        countDownTimer.cancel ();
                        otpView.setText(otp);
                        txtResend.setVisibility ( View.GONE );
                        code = otp;
                        indicatorView.smoothToShow();
                        otpVerifyApi();
                    }
                }

                @Override
                public void onOTPTimeOut() {
                    if (countDownTimer !=null){
                        countDownTimer.cancel ();
                        txtResend.setVisibility (View.VISIBLE);
                        txtResend.setText(getResources().getString(R.string.resend_otp));
                        txtResend.setEnabled (true);
                    }
                }
            };
            smsBroadcastReceiver.initOTPListener(otpListener);
            registerReceiver(smsBroadcastReceiver, new IntentFilter( SmsRetriever.SMS_RETRIEVED_ACTION));
        });

        retriever.addOnFailureListener(e -> Toast.makeText(OTPVerifyActivity.this,"Problem to read otp", Toast.LENGTH_SHORT).show());
    }

    public void disablingResendButton() {
        Handler handlers = new Handler();
        handlers.post(() -> {
            txtResend.setEnabled(false);
            txtTimer.setVisibility ( View.GONE );
            txtResend.setVisibility ( View.VISIBLE );
            countDownTimer = new CountDownTimer(120000, 1000){
                public void onTick(long millisUntilFinished) {
                    int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                    int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);

                    String timeRemaining = Long.toString(millisUntilFinished / 1000);
                    txtResend.setText(String.format("TIMER: %2d:%2d", minutes, seconds));
                }
                public void onFinish() {
                    indicatorView.smoothToHide();
                    txtResend.setVisibility ( View.VISIBLE );
                    txtTimer.setVisibility ( View.GONE );
                    txtResend.setEnabled(true);
                    txtResend.setText(getResources().getString(R.string.resend_otp));
                }
            }.start();
        });
    }

    private void otpVerifyApi() {
        if (AppUtils.isNetworkAvailable(context)) {
            String mPin = otpView.getText().toString().trim();
            ApiManger apiManager = new ApiManger(context, false);
            ApiCallBack<OTPVerifyModel> callBack = new ApiCallBack<>(new ApiResponseListener<OTPVerifyModel>() {
                @Override
                public void onApiSuccess(OTPVerifyModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                    //        Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_LONG).show();
                            SavedPrefManager.saveStringPreferences(context, Constant.ACCESS_TOKEN, response.getToken());
                            Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    indicatorView.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    indicatorView.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    indicatorView.smoothToHide();
                }
            }, Constant.OTP_VERIFY_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", mEmail);
            jsonObject.addProperty("otp", mPin);
            apiManager.otpVerifyApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    private void resendOTPMethod() {
        if (AppUtils.isNetworkAvailable(context)) {
            disablingResendButton();
            ApiManger apiManager = new ApiManger(context, false);
            ApiCallBack<ForgotPasswordModel> callBack = new ApiCallBack<>(new ApiResponseListener<ForgotPasswordModel>() {
                @Override
                public void onApiSuccess(ForgotPasswordModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            retrivedOTPFromDevice();
                    //        Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    indicatorView.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    indicatorView.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    indicatorView.smoothToHide();
                }
            }, Constant.FORGOT_PASSWORD_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", mEmail);
            apiManager.forgotPasswordApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    public boolean validate() {
        boolean valid = true;
        String mPin = otpView.getText().toString().trim();
        if (mPin.isEmpty() || !AppUtils.isOTPValidFourDigit(mPin)) {
            otpView.setError("Enter your OTP");
            valid = false;
        } else {
            otpView.setError(null);
        }

        return valid;
    }
}
