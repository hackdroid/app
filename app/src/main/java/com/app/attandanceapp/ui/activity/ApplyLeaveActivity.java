package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.adapter.LeaveTypeAdapter;
import com.app.attandanceapp.ui.listener.LeaveTypeListener;
import com.app.attandanceapp.ui.model.InboxModel;
import com.app.attandanceapp.ui.model.LeaveType;
import com.app.attandanceapp.ui.model.LeaveTypeModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastAlert;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ApplyLeaveActivity extends AppCompatActivity
        implements View.OnClickListener, LeaveTypeListener {
    private AVLoadingIndicatorView customIndicator;
    Context context;
    Toolbar toolbar;
    Button btnApply;
    String dateToStrCurrent, strCheckIn, strCheckOut,
            TAG = ApplyLeaveActivity.class.getSimpleName();
    ToastAlert alert;
    List<LeaveType> listOfUserAccessType;
    RecyclerView recyclerView;
    private int mYear, mMonth, mDay, leaveTypeId;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    TextView txtToDate, txtFromDate;
    EditText edtNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave);

        context = this;
        alert = new ToastAlert(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_white_24dp);
        toolbar.setTitle("Apply Leave");
        toolbar.setTitleTextColor(-1);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        initViews();
        getCurrentDateAndTIme();
        customIndicator.smoothToShow();
        getLeaveType();

    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        btnApply = findViewById(R.id.btn_apply);
        edtNotes = findViewById(R.id.edt_notes);
        CardView cardViewFromDate = findViewById(R.id.card_start_date);
        CardView cardViewToDate = findViewById(R.id.card_to_date);

        txtFromDate = findViewById(R.id.txt_from_date);
        txtToDate = findViewById(R.id.txt_to_date);
        customIndicator = findViewById(R.id.avi);
        customIndicator.smoothToHide();

        recyclerView = findViewById(R.id.rv_leave_type);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        btnApply.setOnClickListener(this);
        cardViewFromDate.setOnClickListener(this);
        cardViewToDate.setOnClickListener(this);
    }

    private void getCurrentDateAndTIme() {
        try {
            Date today = new Date();
            SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd, hh:mm:ss a");
            dateToStrCurrent = formats.format(today);
        //    String[] dateArrayCurrent = dateToStrCurrent.split(",");
        //    currentDate = dateArrayCurrent[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLeaveType() {
        if (AppUtils.isNetworkAvailable(ApplyLeaveActivity.this)) {
            ApiManger apiManager = new ApiManger(ApplyLeaveActivity.this, false);
            ApiCallBack<LeaveTypeModel> callBack = new ApiCallBack<>(new ApiResponseListener<LeaveTypeModel>() {
                @Override
                public void onApiSuccess(LeaveTypeModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            listOfUserAccessType = response.getRecords();
                            if(listOfUserAccessType != null && !listOfUserAccessType.isEmpty()) {
                                LeaveTypeAdapter adapter = new LeaveTypeAdapter(listOfUserAccessType, R.layout.leave_type_item, context);
                                recyclerView.setAdapter(adapter);
                                adapter.setClickListener(ApplyLeaveActivity.this);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(ApplyLeaveActivity.this, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, ApplyLeaveActivity.this);

            apiManager.getLeaveTypeApi(callBack);
        } else {
            ToastUtils.showToastShort(ApplyLeaveActivity.this, Constant.CHECK_CONNECTION);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_apply:
                if (leaveTypeId != 0) {
                  if (strCheckIn != null) {
                      if (strCheckOut == null) {
                          strCheckOut = "";
                      }
                      if (validation()) {
                          customIndicator.smoothToShow();
                          applyLeave();
                      }
                  }else {
                      alert.showError("Select leave date!");
                  }
                } else {
                    alert.showError("Select leave type!");
                }
                break;

            case R.id.card_start_date:
                calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(context, R.style.datePicker,
                        (view, year, month, dayOfMonth) -> {
                            strCheckIn = year + "-" + (month + 1) + "-" + dayOfMonth;
                            txtFromDate.setText(strCheckIn);
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;

            case R.id.card_to_date:
                calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(context, R.style.datePicker,
                        (view, year, month, dayOfMonth) -> {
                            strCheckOut = year + "-" + (month + 1) + "-" + dayOfMonth;
                            txtToDate.setText(strCheckOut);
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }
    }

    @Override
    public void onUserTypeClick(View view, int position) {
        leaveTypeId = listOfUserAccessType.get(position).getLeaveTypeId();
    }

    private void applyLeave() {
        String strNotes = edtNotes.getText().toString();
        if (AppUtils.isNetworkAvailable(ApplyLeaveActivity.this)) {
            ApiManger apiManager = new ApiManger(ApplyLeaveActivity.this, true);
            ApiCallBack<InboxModel> callBack = new ApiCallBack<>(new ApiResponseListener<InboxModel>() {
                @Override
                public void onApiSuccess(InboxModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            alert.showWarning(response.getMsg());
                            finish();
                        } else {
                            alert.showError(response.getMsg());
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(ApplyLeaveActivity.this, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, ApplyLeaveActivity.this);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("startdate", strCheckIn);
            jsonObject.addProperty("enddate", strCheckOut);
            jsonObject.addProperty("time", dateToStrCurrent);
            jsonObject.addProperty("remarks", strNotes);
            jsonObject.addProperty("type", leaveTypeId);
            apiManager.createNewLeaveApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(ApplyLeaveActivity.this, Constant.CHECK_CONNECTION);
        }
    }

    private Boolean validation() {
        boolean isValid = true;
        String strNotes = edtNotes.getText().toString();
        if (strNotes.isEmpty()) {
            edtNotes.setError(getResources().getString(R.string.enter_reason));
            isValid = false;
        } else {
            edtNotes.setError(null);
        }
        return isValid;
    }
}