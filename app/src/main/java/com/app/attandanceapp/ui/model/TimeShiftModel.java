package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeShiftModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("time")
    @Expose
    private TimeModel time;
    @SerializedName("attandanceapp")
    @Expose
    private AttandanceModel attandance;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public TimeModel getTime() {
        return time;
    }

    public void setTime(TimeModel time) {
        this.time = time;
    }

    public AttandanceModel getAttandance() {
        return attandance;
    }

    public void setAttandance(AttandanceModel attandance) {
        this.attandance = attandance;
    }
}
