package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.model.ResetPasswordModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

public class ChangePasswordActivity extends AppCompatActivity
        implements View.OnClickListener {
    private TextInputEditText edtPassword;
    private AVLoadingIndicatorView customIndicator;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        context = this;

        initViews();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews(){
        RelativeLayout rlFacebook = findViewById(R.id.rl_login);

        edtPassword = findViewById(R.id.password_input);
        customIndicator = findViewById(R.id.avi);
        customIndicator.smoothToHide();

        rlFacebook.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_login:
                if (validate()) {
                    customIndicator.smoothToShow();
                    loginMethod();
                }
                break;
        }
    }

    private void loginMethod() {
        if (AppUtils.isNetworkAvailable(context)) {
            String mPassword = edtPassword.getText().toString().trim();
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<ResetPasswordModel> callBack = new ApiCallBack<>(new ApiResponseListener<ResetPasswordModel>() {
                @Override
                public void onApiSuccess(ResetPasswordModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                    //        Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.CHANGE_PASSWORD_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("password", mPassword);
            apiManager.resetPasswordApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    public boolean validate() {
        boolean valid = true;
        String mPassword = edtPassword.getText().toString().trim();

        if (mPassword.isEmpty() || !AppUtils.isValidPassword(mPassword)) {
            edtPassword.setError("Enter valid password");
            valid = false;
        } else {
            edtPassword.setError(null);
        }

        return valid;
    }
}