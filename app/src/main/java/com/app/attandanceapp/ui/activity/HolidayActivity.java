package com.app.attandanceapp.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.adapter.HolidayListAdapter;
import com.app.attandanceapp.ui.listener.HolidayListener;
import com.app.attandanceapp.ui.model.HolidayModel;
import com.app.attandanceapp.ui.model.HolidayResponse;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastAlert;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Calendar;
import java.util.List;

public class HolidayActivity extends AppCompatActivity implements HolidayListener {
    ImageView imageTuntun;
    Context context;
    private AVLoadingIndicatorView customIndicator;
    RecyclerView recyclerView;
    List<HolidayResponse> listOfHomeData;
    ToastAlert alert;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holiday);

        context = this;
        alert = new ToastAlert(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_white_24dp);
        toolbar.setTitle("Holiday List");
        toolbar.setTitleTextColor(-1);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        initViews();

        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);

        customIndicator.smoothToShow();
        getHolidayDataFromServer(month + 1);
    }

    private void initViews() {
        recyclerView = findViewById(R.id.rl_quiz);
        customIndicator = findViewById(R.id.avi);
        imageTuntun = findViewById(R.id.tuntun);
        customIndicator.smoothToHide();

        recyclerView.setHasFixedSize(true);
    //    StaggeredGridLayoutManager gaggeredGridLayoutManager = new StaggeredGridLayoutManager(5, 1);
    //    recyclerView.setLayoutManager(gaggeredGridLayoutManager);
    }

    private void getHolidayDataFromServer(final int noOfMonth) {
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, false);
            ApiCallBack<HolidayModel> callBack = new ApiCallBack<>(new ApiResponseListener<HolidayModel>() {
                @Override
                public void onApiSuccess(HolidayModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            Toast.makeText(context, response.getMsg(), Toast.LENGTH_LONG).show();
                            listOfHomeData = response.getRecords();
                            if (listOfHomeData != null && !listOfHomeData.isEmpty()) {
                                imageTuntun.setVisibility(View.GONE);
                                HolidayListAdapter adapter = new HolidayListAdapter(listOfHomeData, R.layout.holiday_list_item, context);
                                recyclerView.setAdapter(adapter);
                                adapter.setClickListener(HolidayActivity.this::onHolidayClickListener);
                            } else {
                                imageTuntun.setVisibility(View.VISIBLE);
                            }
                        }else {
                            Toast.makeText(context.getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, context);
            apiManager.getHolidayListApi(callBack, noOfMonth);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    @Override
    public void onHolidayClickListener(View view, int position) {
        if (listOfHomeData.get(position).getCalendarIsHoliday()) {
            alert.showWarning(listOfHomeData.get(position).getCalendarLabel());
        }
    }
}
