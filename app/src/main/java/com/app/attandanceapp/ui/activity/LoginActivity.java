package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.helper.SavedPrefManager;
import com.app.attandanceapp.helper.SessionManager;
import com.app.attandanceapp.notifications.Config;
import com.app.attandanceapp.ui.model.LoginModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastAlert;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private AVLoadingIndicatorView customIndicator;
    ToastAlert alert;
    TextInputEditText emailInput, pwdInput;
    SessionManager sessionManager;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;

        alert = new ToastAlert(this);
        sessionManager = new SessionManager(this);
        getRegId();

        initViews();
    }

    private String getRegId(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        return pref.getString("regId",null);
    }

    private void initViews() {
        emailInput = findViewById(R.id.email_input);
        pwdInput = findViewById(R.id.password_input);
        Button loginBtn = findViewById(R.id.login_btn);
        TextView txtForgot = findViewById(R.id.txt_forget);
        customIndicator = findViewById(R.id.avi);
        customIndicator.smoothToHide();

        loginBtn.setOnClickListener(this);
        txtForgot.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                if (validate()) {
                    String strToken = getRegId();
                    if (strToken == null) {
                        getTokenFromFirebase();
                        strToken = getRegId();
                    }
                    customIndicator.smoothToShow();
                    loginMethod(strToken);
                }
                break;
            case R.id.txt_forget:
                forgotMethod();
                break;
        }
    }

    private void getTokenFromFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String token = task.getResult().getToken();
                storeRegIdInPref(token);
            }
        });
    }
    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        //     Log.d(TAG, "storeRegIdInPref: regId : " + token);
        editor.apply();
    }

    private void loginMethod(final String token) {
        if (AppUtils.isNetworkAvailable(context)) {
            String mEmail = emailInput.getText().toString().trim();
            String mPassword = pwdInput.getText().toString().trim();
            ApiManger apiManager = new ApiManger(context, false);
            ApiCallBack<LoginModel> callBack = new ApiCallBack<>(new ApiResponseListener<LoginModel>() {
                @Override
                public void onApiSuccess(LoginModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                        //    Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_LONG).show();
                            sessionManager.setLogin(true);
                            SavedPrefManager.saveStringPreferences(context, Constant.ACCESS_TOKEN, response.getToken());
                            SavedPrefManager.saveStringPreferences(context, Constant.USER_EMAIL, mEmail);
                            SavedPrefManager.saveStringPreferences(context, Constant.USER_NAME, response.getUser().getUserName());
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", mEmail);
            jsonObject.addProperty("password", mPassword);
            jsonObject.addProperty("fcm", token);
            apiManager.loginApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }
    private void forgotMethod(){
        Intent attandanceMarking = new Intent(getApplicationContext(), ForgotActivity.class);
        startActivity(attandanceMarking);
    }

    public boolean validate() {
        boolean valid = true;
        String mEmail = emailInput.getText().toString().trim();
        String mPassword = pwdInput.getText().toString().trim();

        if (mEmail.isEmpty() || !AppUtils.isEmailValid(mEmail)) {
            emailInput.setError(getResources().getString(R.string.valid_email));
            valid = false;
        } else {
            emailInput.setError(null);
        }

        if (mPassword.isEmpty() || !AppUtils.isValidPassword(mPassword)) {
            pwdInput.setError(getResources().getString(R.string.error_valid_pass));
            valid = false;
        } else {
            pwdInput.setError(null);
        }
        return valid;
    }
}