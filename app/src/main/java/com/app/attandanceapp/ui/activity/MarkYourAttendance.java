package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.helper.SavedPrefManager;
import com.app.attandanceapp.ui.model.CheckOutModel;
import com.app.attandanceapp.ui.model.EncryptModel;
import com.app.attandanceapp.ui.model.LatitudeModel;
import com.app.attandanceapp.ui.model.LongitudeModel;
import com.app.attandanceapp.ui.model.MarkAttendanceModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.LocationTrack;
import com.app.attandanceapp.utils.ToastAlert;
import com.app.attandanceapp.utils.ToastUtils;
import com.app.attandanceapp.utils.WifiBroadcastReceiver;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * We will need shift timing
 * starting time = 9:00AM
 */
public class MarkYourAttendance extends AppCompatActivity
        implements View.OnClickListener{
    private AVLoadingIndicatorView customIndicator;
    Context context;
    Toolbar toolbar;
    TextClock textClock;
    Button clockIn, clockOut;
    String strUserEmail, currentDate, dateToStrCurrent, strIpAddress, desiredMAC, strUserName,
            TAG = MarkYourAttendance.class.getSimpleName();
    boolean isAtLocation = false ;
    ToastAlert alert;
    TextView txtDate;
    EditText edtTime;
    CardView cardViewDate;
    int intAttendanceType;
    LocationTrack locationTrack;
    String strCompleteAddress;
    double longitude, latitude;
    public final static int TAG_PERMISSION_CODE = 1;
    LatitudeModel latitudeModel;
    LongitudeModel longitudeModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attandance_mark);

        context = getApplicationContext();
        alert = new ToastAlert(this);
        desiredMAC = context.getString(R.string.mac_address);
        /*BroadcastReceiver broadcastReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        context.registerReceiver(broadcastReceiver, filter);*/

        checkWifiConnection();BroadcastReceiver broadcastReceiver = new WifiBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        context.registerReceiver(broadcastReceiver, filter);

    //    int ipAddress = wifiInf.getIpAddress();
    //    strIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_white_24dp);
        toolbar.setTitle("Mark Attendance");
        toolbar.setTitleTextColor(-1);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        strUserEmail = SavedPrefManager.getStringPreferences(context, Constant.USER_EMAIL);
        strUserName = SavedPrefManager.getStringPreferences(context, Constant.USER_NAME);
        intAttendanceType = getIntent().getIntExtra("intAttendanceType", 0);

        initViews();
        getCurrentDateAndTIme();
        checkLocPermission();
    }

    private void checkWifiConnection() {
        WifiManager wifiMan = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();

        if (wifiInf != null) {
            strIpAddress = wifiInf.getBSSID();
            Log.d(TAG, "checkConnectedForWifiDevice: " + strIpAddress);
        } else {
            Log.d(TAG, "checkConnectedForWifiDevice: Not connected to wifi");
        }
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        textClock = findViewById(R.id.text_clock);
        clockIn = findViewById(R.id.btn_clock_in);
        clockOut = findViewById(R.id.btn_clock_out);
        txtDate = findViewById(R.id.txt_date);
        edtTime = findViewById(R.id.edt_time);
        cardViewDate = findViewById(R.id.card_date);
        TextView txtUserName = findViewById(R.id.txt_user_name);
        customIndicator = findViewById(R.id.avi);
        customIndicator.smoothToHide();

        clockIn.setOnClickListener(this);
        clockOut.setOnClickListener(this);
        txtUserName.setText(strUserName);

        if (intAttendanceType == 1) {
            clockIn.setVisibility(View.VISIBLE);
            clockOut.setVisibility(View.GONE);
        } else {
            clockIn.setVisibility(View.GONE);
            clockOut.setVisibility(View.VISIBLE);
        }

        if (strUserEmail.equals(Constant.RAVINDER_EMAIL)
                || strUserEmail.equals(Constant.PREETI_EMAIL)
                || strUserEmail.equals(Constant.KHALID_EMAIL)) {
            cardViewDate.setVisibility(View.VISIBLE);
        } else {
            cardViewDate.setVisibility(View.GONE);
        }
    }

    private void getCurrentDateAndTIme() {
        try {
            Date today = new Date();
            SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd, hh:mm:ss a");
            SimpleDateFormat format = new SimpleDateFormat("dd MMM, hh:mm:ss a");
            String dateToStr = format.format(today);
            dateToStrCurrent = formats.format(today);
            String[] dateArray = dateToStr.split(",");
            String[] dateArrayCurrent = dateToStrCurrent.split(",");
            String strDate = dateArray[0];
            currentDate = dateArrayCurrent[0];
            String strTIme = dateArray[1];
            txtDate.setText(strDate);
            edtTime.setText(strTIme);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void checkLocPermission() {
        if(!AppUtils.checkPermission(MarkYourAttendance.this)) {
           // AppUtils.requestPermission(MarkYourAttendance.this, TAG_PERMISSION_CODE);
        //    validateLeadPopup();
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", getPackageName(), null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            getCurrentLocation();
        }
    }

    public void validateLeadPopup(){
        new IOSDialog.Builder(MarkYourAttendance.this)
                .setTitle(getResources().getString(R.string.popup_confirmation))
                .setMessage(getResources().getString(R.string.add_more_popup))
                .setPositiveButton(getResources().getString(R.string.popup_yes),
                        (dialog, which) -> {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            dialog.dismiss();
                        })
                .setNegativeButton(getResources().getString(R.string.popup_no),
                        (dialog, which) -> {
                            finish();
                            dialog.dismiss();
                        })
                .show();
    }

    private void getCurrentLocation(){
        locationTrack = new LocationTrack(MarkYourAttendance.this);
        if (locationTrack.canGetLocation()) {
            longitude = locationTrack.getLongitude();
            latitude = locationTrack.getLatitude();
            String longitudes = String.valueOf(longitude);
            String latitudes = String.valueOf(latitude);
            if (!longitudes.isEmpty()){
                getAddress(context, Double.parseDouble(latitudes), Double.parseDouble(longitudes));
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
            }
        } else {
            locationTrack.showSettingsAlert();
        }
    }

    public void getAddress(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                //    adminAreaState = addresses.get(0).getAdminArea();
                //    countryName = addresses.get(0).getCountryName();
                //    aubAdminAreaDistrict = addresses.get(0).getSubAdminArea();
                //    postalCode = addresses.get(0).getPostalCode();
                strCompleteAddress = addresses.get(0).getAddressLine(0);
                customIndicator.smoothToShow();
                encryptLatLong();
                //    txtAddress.setText(strCompleteAddress);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void encryptLatLong() {
        if (AppUtils.isNetworkAvailable(MarkYourAttendance.this)) {
            ApiManger apiManager = new ApiManger(MarkYourAttendance.this, false);
            ApiCallBack<EncryptModel> callBack = new ApiCallBack<>(new ApiResponseListener<EncryptModel>() {
                @Override
                public void onApiSuccess(EncryptModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            latitudeModel = response.getData().getLatitude();
                            longitudeModel = response.getData().getLongitude();
                    //        Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(MarkYourAttendance.this, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, MarkYourAttendance.this);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("latitude", String.valueOf(latitude));
            jsonObject.addProperty("longitude", String.valueOf(longitude));
            apiManager.latLongEncryptApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(MarkYourAttendance.this, Constant.CHECK_CONNECTION);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_clock_in:
                Log.d(TAG, "Presence marking: ");
                if (String.valueOf(latitude).equals("0.0")) {
                    checkLocPermission();
                } else {
                  try{

                      assert strIpAddress != null;
                      if (strIpAddress.equals(desiredMAC)) {
                          customIndicator.smoothToShow();
                          markAttendance();
                          //    Toast.makeText(getApplicationContext(), strIpAddress, Toast.LENGTH_SHORT).show();
                      } else {
                          checkWifiConnection();
                          alert.showNoInternet(getString(R.string.not_in_campus));
                      }
                  }catch (Exception e){
//
                  }
                    /*isAtLocation = Constant.isConnectedtoWIFI;
                    if (!isAtLocation){
                        alert.showNoInternet(getString(R.string.not_in_campus));
                        return;
                    }*/
                   // customIndicator.smoothToShow();
                   // markAttendance();
                }
                break;

            case R.id.btn_clock_out:
                Log.d(TAG, "Presence marking: ");
                /*isAtLocation = Constant.isConnectedtoWIFI;
                if (!isAtLocation){
                    alert.showNoInternet(getString(R.string.not_in_campus));
                    return;
                }*/
                assert strIpAddress != null;
                if (strIpAddress.equals(desiredMAC)) {
                    customIndicator.smoothToShow();
                    checkOutAttendance();
                    //    Toast.makeText(getApplicationContext(), strIpAddress, Toast.LENGTH_SHORT).show();
                } else {
                    checkWifiConnection();
                    alert.showNoInternet(getString(R.string.not_in_campus));
                }
                break;
        }
    }

    private void markAttendance() {
        if (AppUtils.isNetworkAvailable(MarkYourAttendance.this)) {
            ApiManger apiManager = new ApiManger(MarkYourAttendance.this, true);
            ApiCallBack<MarkAttendanceModel> callBack = new ApiCallBack<>(new ApiResponseListener<MarkAttendanceModel>() {
                @Override
                public void onApiSuccess(MarkAttendanceModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            alert.showWarning(response.getMsg());
                            clockIn.setVisibility(View.GONE);
                            clockOut.setVisibility(View.VISIBLE);
                            finish();
                        } else {
                            alert.showError(response.getMsg());
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(MarkYourAttendance.this, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, MarkYourAttendance.this);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("type", intAttendanceType);
            jsonObject.addProperty("time", currentDate + " " + edtTime.getText().toString());
            jsonObject.addProperty("date", currentDate);
            jsonObject.addProperty("address", strCompleteAddress);

            JsonObject latitudeObject = new JsonObject();
            latitudeObject.addProperty("iv", latitudeModel.getIv());
            latitudeObject.addProperty("content", latitudeModel.getContent());
            jsonObject.add("latitude", latitudeObject);

            JsonObject longitudeObject = new JsonObject();
            longitudeObject.addProperty("iv", longitudeModel.getIv());
            longitudeObject.addProperty("content", longitudeModel.getContent());
            jsonObject.add("longitude", longitudeObject);

            apiManager.markAttendanceApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(MarkYourAttendance.this, Constant.CHECK_CONNECTION);
        }
    }

    private void checkOutAttendance() {
        if (AppUtils.isNetworkAvailable(MarkYourAttendance.this)) {
            ApiManger apiManager = new ApiManger(MarkYourAttendance.this, true);
            ApiCallBack<CheckOutModel> callBack = new ApiCallBack<>(new ApiResponseListener<CheckOutModel>() {
                @Override
                public void onApiSuccess(CheckOutModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            alert.showWarning(response.getMsg());
                            clockOut.setVisibility(View.GONE);
                            finish();
                        } else {
                            alert.showError(response.getMsg());
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(MarkYourAttendance.this, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, MarkYourAttendance.this);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("time", currentDate + " " + edtTime.getText().toString());
            jsonObject.addProperty("date", currentDate);
            apiManager.checkOutApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(MarkYourAttendance.this, Constant.CHECK_CONNECTION);
        }
    }
}