package com.app.attandanceapp.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.activity.HomeActivity;
import com.app.attandanceapp.ui.activity.MarkYourAttendance;
import com.app.attandanceapp.ui.adapter.AttendanceListAdapter;
import com.app.attandanceapp.ui.model.AttendanceListModel;
import com.app.attandanceapp.ui.model.AttendanceRecord;
import com.app.attandanceapp.ui.model.TimeShiftModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class HomeFragment extends Fragment
        implements View.OnClickListener {
    Context context;
    private AVLoadingIndicatorView customIndicator;
    int intAttendanceType;
    RecyclerView recyclerView;
    ImageView imageViewNoData;
    String strDate;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_gragment, container, false);
        ((HomeActivity) getActivity()).setTitle(getResources().getString(R.string.title_home));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        FloatingActionButton markPresence = view.findViewById(R.id.mark_your_presence);
        recyclerView = view.findViewById(R.id.rl_quiz);
        customIndicator = view.findViewById(R.id.avi);
        imageViewNoData = view.findViewById(R.id.cal);
        customIndicator.smoothToHide();

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));

        markPresence.setOnClickListener(this);
        getCurrentDateAndTIme();
    }

    @Override
    public void onResume() {
        super.onResume();
        customIndicator.smoothToShow();
        getHomeDataFromServer();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mark_your_presence:
                customIndicator.smoothToShow();
                getTimeShiftFromServer(strDate);
                break;
        }
    }

    private void getCurrentDateAndTIme() {
        try {
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd, hh:mm:ss a");
           // SimpleDateFormat format = new SimpleDateFormat("dd MMM, hh:mm:ss a");
            String dateToStr = format.format(today);
            String[] dateArray = dateToStr.split(",");
            strDate = dateArray[0];
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getTimeShiftFromServer(final String strDate){
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<TimeShiftModel> callBack = new ApiCallBack<>(new ApiResponseListener<TimeShiftModel>() {
                @Override
                public void onApiSuccess(TimeShiftModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                    //        Toast.makeText(context, response.getMsg(), Toast.LENGTH_LONG).show();
                            if (response.getAttandance() != null){
                                intAttendanceType = 2;
                            } else {
                                intAttendanceType = 1;
                            }
                            Intent markYourPresence = new Intent(context , MarkYourAttendance.class);
                            markYourPresence.putExtra("intAttendanceType", intAttendanceType);
                            startActivity(markYourPresence);
                        } else {
                            Toast.makeText(context, response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("date", strDate);
            apiManager.timeShiftApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    private void getHomeDataFromServer() {
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<AttendanceListModel> callBack = new ApiCallBack<>(new ApiResponseListener<AttendanceListModel>() {
                @Override
                public void onApiSuccess(AttendanceListModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            List<AttendanceRecord> listOfHomeData = response.getRecords();
                            if (listOfHomeData != null && !listOfHomeData.isEmpty()) {
                                imageViewNoData.setVisibility(View.GONE);
                                AttendanceListAdapter adapter = new AttendanceListAdapter(listOfHomeData, R.layout.attendance_list_item, context);
                                recyclerView.setAdapter(adapter);
                            } else {
                                imageViewNoData.setVisibility(View.VISIBLE);
                            }
                        }else {
                            Toast.makeText(context.getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("startdate", "");
            jsonObject.addProperty("enddate", "");
            apiManager.getAttendanceListApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }
}