package com.app.attandanceapp.ui.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.model.ForgotPasswordModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

public class ForgotActivity extends AppCompatActivity
        implements View.OnClickListener {
    TextInputEditText emailInput;
    private AVLoadingIndicatorView customIndicator;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = this;
        initViews();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews(){
        RelativeLayout rlFacebook = findViewById(R.id.rl_login);
        emailInput = findViewById(R.id.email_input);
        customIndicator = findViewById(R.id.avi);
        customIndicator.smoothToHide();

        rlFacebook.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_login:
                if (validate()){
                    customIndicator.smoothToShow();
                    forgotPassMethod();
                }
                break;
        }
    }

    private void forgotPassMethod() {
        if (AppUtils.isNetworkAvailable(context)) {
            String mEmail = emailInput.getText().toString().trim();
            ApiManger apiManager = new ApiManger(context, false);
            ApiCallBack<ForgotPasswordModel> callBack = new ApiCallBack<>(new ApiResponseListener<ForgotPasswordModel>() {
                @Override
                public void onApiSuccess(ForgotPasswordModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            Intent intent = new Intent(getApplicationContext(), OTPVerifyActivity.class);
                            intent.putExtra("email", mEmail);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, getResources().getString(R.string.msg_incorrect_info));
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.FORGOT_PASSWORD_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", mEmail);
            apiManager.forgotPasswordApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    public boolean validate() {
        boolean valid = true;
        String mEmail = emailInput.getText().toString().trim();

        if (mEmail.isEmpty() || !AppUtils.isEmailValid(mEmail)) {
            emailInput.setError(getResources().getString(R.string.valid_email));
            valid = false;
        } else {
            emailInput.setError(null);
        }

        return valid;
    }
}

