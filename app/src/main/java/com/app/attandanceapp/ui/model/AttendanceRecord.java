package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceRecord {
    @SerializedName("attandance_id")
    @Expose
    private Integer attandanceId;
    @SerializedName("attandance_user")
    @Expose
    private Integer attandanceUser;
    @SerializedName("attandance_date")
    @Expose
    private String attandanceDate;
    @SerializedName("attandance_time")
    @Expose
    private String attandanceTime;
    @SerializedName("attandance_latitude")
    @Expose
    private String attandanceLatitude;
    @SerializedName("attandance_longitude")
    @Expose
    private String attandanceLongitude;
    @SerializedName("attandance_address")
    @Expose
    private String attandanceAddress;
    @SerializedName("attandance_distance")
    @Expose
    private String attandanceDistance;
    @SerializedName("attandance_check_out")
    @Expose
    private String attandanceCheckOut;
    @SerializedName("attandance_type")
    @Expose
    private Integer attandanceType;
    @SerializedName("type")
    @Expose
    private AttendanceType type;

    public Integer getAttandanceId() {
        return attandanceId;
    }

    public void setAttandanceId(Integer attandanceId) {
        this.attandanceId = attandanceId;
    }

    public Integer getAttandanceUser() {
        return attandanceUser;
    }

    public void setAttandanceUser(Integer attandanceUser) {
        this.attandanceUser = attandanceUser;
    }

    public String getAttandanceDate() {
        return attandanceDate;
    }

    public void setAttandanceDate(String attandanceDate) {
        this.attandanceDate = attandanceDate;
    }

    public String getAttandanceTime() {
        return attandanceTime;
    }

    public void setAttandanceTime(String attandanceTime) {
        this.attandanceTime = attandanceTime;
    }

    public String getAttandanceLatitude() {
        return attandanceLatitude;
    }

    public void setAttandanceLatitude(String attandanceLatitude) {
        this.attandanceLatitude = attandanceLatitude;
    }

    public String getAttandanceLongitude() {
        return attandanceLongitude;
    }

    public void setAttandanceLongitude(String attandanceLongitude) {
        this.attandanceLongitude = attandanceLongitude;
    }

    public String getAttandanceAddress() {
        return attandanceAddress;
    }

    public void setAttandanceAddress(String attandanceAddress) {
        this.attandanceAddress = attandanceAddress;
    }

    public String getAttandanceDistance() {
        return attandanceDistance;
    }

    public void setAttandanceDistance(String attandanceDistance) {
        this.attandanceDistance = attandanceDistance;
    }

    public String getAttandanceCheckOut() {
        return attandanceCheckOut;
    }

    public void setAttandanceCheckOut(String attandanceCheckOut) {
        this.attandanceCheckOut = attandanceCheckOut;
    }

    public Integer getAttandanceType() {
        return attandanceType;
    }

    public void setAttandanceType(Integer attandanceType) {
        this.attandanceType = attandanceType;
    }

    public AttendanceType getType() {
        return type;
    }

    public void setType(AttendanceType type) {
        this.type = type;
    }
}
