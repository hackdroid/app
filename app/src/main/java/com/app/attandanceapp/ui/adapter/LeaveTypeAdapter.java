package com.app.attandanceapp.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.attandanceapp.R;
import com.app.attandanceapp.ui.listener.LeaveTypeListener;
import com.app.attandanceapp.ui.model.LeaveType;

import java.util.List;

public class LeaveTypeAdapter extends RecyclerView.Adapter<LeaveTypeAdapter.CustomViewHolder> {
    private static final String TAG = LeaveTypeAdapter.class.getSimpleName();
    private List<LeaveType> menuCategoryList;
    private Context context;
    private int rowLayout, lastPosition;
    private LeaveTypeListener clickListener;
    private int row_index = -1;

    public LeaveTypeAdapter(List<LeaveType> menuCategoryList, int rowLayout, Context context) {
        this.menuCategoryList = menuCategoryList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        ((CustomViewHolder) holder).categoryName.setText(menuCategoryList.get(position).getLeaveTypeTitle());
        ((CustomViewHolder) holder).relativeLayout.setOnClickListener((View.OnClickListener) view -> {
            row_index = position;
            notifyDataSetChanged();
            if (clickListener != null) clickListener.onUserTypeClick(view, position);
        });
        if(row_index == position) {
            ((CustomViewHolder) holder).relativeLayout.setBackgroundResource(R.drawable.leave_type_selected);
            ((CustomViewHolder) holder).categoryName.setTextColor(Color.parseColor("#ffffff"));
        } else {
            ((CustomViewHolder) holder).relativeLayout.setBackgroundResource(R.drawable.leave_type_normal);
            ((CustomViewHolder) holder).categoryName.setTextColor(Color.parseColor("#000000"));
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        RelativeLayout relativeLayout;

        public CustomViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.txt_type);
            relativeLayout = view.findViewById(R.id.rl_bg);
            view.setTag(view);
        }
    }

    public void setClickListener(LeaveTypeListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount()  {
        return menuCategoryList == null ? 0 : menuCategoryList.size();
    }
}