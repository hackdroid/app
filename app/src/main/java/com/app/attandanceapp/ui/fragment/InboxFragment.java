package com.app.attandanceapp.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.ui.activity.ApplyLeaveActivity;
import com.app.attandanceapp.ui.activity.HomeActivity;
import com.app.attandanceapp.ui.adapter.InboxListAdapter;
import com.app.attandanceapp.ui.model.InboxModel;
import com.app.attandanceapp.ui.model.InboxRecordModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.ToastUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;


public class InboxFragment extends Fragment implements View.OnClickListener {
    ImageView imageTuntun;
    Context context;
    private AVLoadingIndicatorView customIndicator;
    int intAttendanceType;
    RecyclerView recyclerView;

    public InboxFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        ((HomeActivity) getActivity()).setTitle(getResources().getString(R.string.title_inbox));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        FloatingActionButton markPresence = view.findViewById(R.id.mark_your_presence);
        recyclerView = view.findViewById(R.id.rl_quiz);
        customIndicator = view.findViewById(R.id.avi);
        imageTuntun = view.findViewById(R.id.tuntun);
        customIndicator.smoothToHide();

        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));

        markPresence.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        customIndicator.smoothToShow();
        getInboxDataFromServer();
    }

    private void getInboxDataFromServer() {
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<InboxModel> callBack = new ApiCallBack<>(new ApiResponseListener<InboxModel>() {
                @Override
                public void onApiSuccess(InboxModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                    //        Toast.makeText(context, response.getMsg(), Toast.LENGTH_LONG).show();
                            List<InboxRecordModel> listOfHomeData = response.getRecords();
                            if (listOfHomeData != null && !listOfHomeData.isEmpty()) {
                                imageTuntun.setVisibility(View.GONE);
                                InboxListAdapter adapter = new InboxListAdapter(listOfHomeData, R.layout.inbox_list_item, context);
                                recyclerView.setAdapter(adapter);
                            } else {
                                imageTuntun.setVisibility(View.VISIBLE);
                            }
                        }else {
                            Toast.makeText(context.getApplicationContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    customIndicator.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    customIndicator.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    customIndicator.smoothToHide();
                }
            }, Constant.LOGIN_API, context);
            apiManager.getLeaveHistoryApi(callBack);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mark_your_presence:
                Intent markYourPresence = new Intent(context , ApplyLeaveActivity.class);
                getActivity().startActivity(markYourPresence);
                break;
        }
    }
}