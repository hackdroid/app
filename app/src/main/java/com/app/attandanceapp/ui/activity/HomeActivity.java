package com.app.attandanceapp.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.app.attandanceapp.R;
import com.app.attandanceapp.helper.SessionManager;
import com.app.attandanceapp.ui.fragment.HomeFragment;
import com.app.attandanceapp.ui.fragment.InboxFragment;
import com.app.attandanceapp.ui.fragment.NotificationFragment;
import com.app.attandanceapp.ui.fragment.ProfileFragment;

import static androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

public class HomeActivity extends AppCompatActivity {
    private Context context;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;
    private String FRAGMENT_HOME = "Home", FRAGMENT_OTHER = "Favorite";
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        context = this;
        sessionManager = new SessionManager(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            toolbar.setTitleMarginStart((int) getResources().getDimension(R.dimen._15sdp));
        }

        init();
        toolbar.setTitle(getResources().getString(R.string.title_home));
        viewFragment(new HomeFragment(), FRAGMENT_HOME);
    }

    @SuppressLint("NonConstantResourceId")
    private void init() {
        bottomNavigationView = findViewById(R.id.navigationBottom);
        bottomNavigationView.setSelectedItemId(R.id.home);

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.home:
                    viewFragment(new HomeFragment(), FRAGMENT_HOME);
                    return true;
                case R.id.inbox:
                    viewFragment(new InboxFragment(), FRAGMENT_OTHER);
                    return true;
                case R.id.notification:
                    viewFragment(new NotificationFragment(), FRAGMENT_OTHER);
                    return true;
                case R.id.profile:
                    viewFragment(new ProfileFragment(), FRAGMENT_OTHER);
                    return true;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void viewFragment(Fragment fragment, String name){
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frament_layout, fragment);
        // 1. Know how many fragments there are in the stack
        final int count = fragmentManager.getBackStackEntryCount();
        // 2. If the fragment is **not** "home type", save it to the stack
        if( name.equals( FRAGMENT_OTHER) ) {
            fragmentTransaction.addToBackStack(name);
        }
        // Commit !
        fragmentTransaction.commit();
        // 3. After the commit, if the fragment is not an "home type" the back stack is changed, triggering the
        // OnBackStackChanged callback
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                // If the stack decreases it means I clicked the back button
                if( fragmentManager.getBackStackEntryCount() <= count){
                    // pop all the fragment and remove the listener
                    fragmentManager.popBackStack(FRAGMENT_OTHER, POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.removeOnBackStackChangedListener(this);
                    // set the home button selected
                    bottomNavigationView.getMenu().getItem(0).setChecked(true);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void mLogOut() {
        sessionManager.setLogin(false);
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
/*extends AppCompatActivity {
    FloatingActionButton markPresence ;
    BottomNavigationView dashNav ;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        markPresence = findViewById(R.id.mark_your_presence);
        dashNav = findViewById(R.id.bottomNavigationView);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            toolbar.setTitleMarginStart((int) getResources().getDimension(R.dimen._15sdp));
        }

        toolbar.setTitle(getResources().getString(R.string.title_home));
        *//**
         * Bottom navigation
         *//*
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(dashNav,
                navHostFragment.getNavController());

        markPresence.setOnClickListener(v->{
            Intent markYourPresence = new Intent(getApplicationContext() , MarkYourAttendance.class);
            startActivity(markYourPresence);
        });
    }
}*/