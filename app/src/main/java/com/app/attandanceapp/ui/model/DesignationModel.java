package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesignationModel {
    @SerializedName("designation_id")
    @Expose
    private Integer designationId;
    @SerializedName("designation_title")
    @Expose
    private String designationTitle;
    @SerializedName("designation_ref")
    @Expose
    private Integer designationRef;
    @SerializedName("designation_status")
    @Expose
    private Integer designationStatus;

    public Integer getDesignationId() {
        return designationId;
    }

    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    public String getDesignationTitle() {
        return designationTitle;
    }

    public void setDesignationTitle(String designationTitle) {
        this.designationTitle = designationTitle;
    }

    public Integer getDesignationRef() {
        return designationRef;
    }

    public void setDesignationRef(Integer designationRef) {
        this.designationRef = designationRef;
    }

    public Integer getDesignationStatus() {
        return designationStatus;
    }

    public void setDesignationStatus(Integer designationStatus) {
        this.designationStatus = designationStatus;
    }
}
