package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeaveTypeModel {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("records")
    @Expose
    private List<LeaveType> records = null;
    @SerializedName("msg")
    @Expose
    private String msg;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<LeaveType> getRecords() {
        return records;
    }

    public void setRecords(List<LeaveType> records) {
        this.records = records;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
