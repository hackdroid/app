package com.app.attandanceapp.ui.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.attandanceapp.R;
import com.app.attandanceapp.ui.activity.HomeActivity;


public class NotificationFragment extends Fragment {



    public NotificationFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ((HomeActivity) getActivity()).setTitle(getResources().getString(R.string.title_notification));
        return view;
    }
}