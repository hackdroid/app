package com.app.attandanceapp.ui.listener;

import android.view.View;

public interface HolidayListener {
    void onHolidayClickListener(View view, int position);
}
