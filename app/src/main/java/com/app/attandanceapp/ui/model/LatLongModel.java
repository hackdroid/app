package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LatLongModel {
    @SerializedName("latitude")
    @Expose
    private LatitudeModel latitude;
    @SerializedName("longitude")
    @Expose
    private LongitudeModel longitude;

    public LatitudeModel getLatitude() {
        return latitude;
    }

    public void setLatitude(LatitudeModel latitude) {
        this.latitude = latitude;
    }

    public LongitudeModel getLongitude() {
        return longitude;
    }

    public void setLongitude(LongitudeModel longitude) {
        this.longitude = longitude;
    }
}
