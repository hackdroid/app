package com.app.attandanceapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.attandanceapp.R;
import com.app.attandanceapp.ui.model.InboxRecordModel;
import com.app.attandanceapp.utils.AppUtils;

import java.util.List;

public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.CustomViewHolder> {
    private static final String TAG = InboxListAdapter.class.getSimpleName();
    private List<InboxRecordModel> menuCategoryList;
    private Context context;
    private int rowLayout;

    public InboxListAdapter(List<InboxRecordModel> menuCategoryList, int rowLayout, Context context) {
        this.menuCategoryList = menuCategoryList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        try{
            String strStartDate = AppUtils.convertMongoDateWithS(menuCategoryList.get(position).getLeaveApplicationStartDate());
            String strCreatedDate = AppUtils.convertMongoDateWithS(menuCategoryList.get(position).getLeaveApplicationDate());
            String[] strDateArray = strStartDate.split("\\s+");
            String startDate = strDateArray[0];
            if (menuCategoryList.get(position).getLeaveApplicationEndDate() != null) {
                ((CustomViewHolder)holder).txtLeaveOn.setText("Leave from" + " " + startDate + " " + "To" + " " + menuCategoryList.get(position).getLeaveApplicationEndDate());
            } else {
                ((CustomViewHolder)holder).txtLeaveOn.setText("Leave on" + " " + startDate);
            }

            if (menuCategoryList.get(position).getLeaveApplicationStatus() != null) {
                if (menuCategoryList.get(position).getLeaveApplicationStatus()) {
                    ((CustomViewHolder)holder).txtLeaveStatus.setText("Accepted");
                    ((CustomViewHolder)holder).txtLeaveStatus.getResources().getColor(R.color.teal_700);
                } else {
                    ((CustomViewHolder)holder).txtLeaveStatus.setText("Rejected");
                    ((CustomViewHolder)holder).txtLeaveStatus.getResources().getColor(R.color.error_color);
                }
            } else {
                ((CustomViewHolder)holder).txtLeaveStatus.setText("Pending");
                ((CustomViewHolder)holder).txtLeaveStatus.getResources().getColor(R.color.warning_color);
            }
            ((CustomViewHolder)holder).txtLeaveType.setText(menuCategoryList.get(position).getLeaveType().getLeaveTypeTitle());
            ((CustomViewHolder)holder).txtLeaveCreated.setText(strCreatedDate);
            ((CustomViewHolder)holder).txtLeaveReason.setText(menuCategoryList.get(position).getLeaveApplicationRemarks());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView txtLeaveType, txtLeaveReason, txtLeaveOn, txtLeaveStatus, txtLeaveCreated;
        public CustomViewHolder(View view) {
            super(view);
            txtLeaveType = view.findViewById(R.id.txt_leave_type);
            txtLeaveReason = view.findViewById(R.id.txt_leave_reason);
            txtLeaveStatus = view.findViewById(R.id.txt_leave_status);
            txtLeaveOn = view.findViewById(R.id.txt_leave_on);
            txtLeaveCreated = view.findViewById(R.id.txt_leave_created);
            view.setTag(view);
        }
    }

    @Override
    public int getItemCount()  {
        return menuCategoryList == null ? 0 : menuCategoryList.size();
    }
}