package com.app.attandanceapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.attandanceapp.R;
import com.app.attandanceapp.ui.model.AttendanceRecord;
import com.app.attandanceapp.utils.AppUtils;

import java.util.List;

public class AttendanceListAdapter extends RecyclerView.Adapter<AttendanceListAdapter.CustomViewHolder> {
    private static final String TAG = AttendanceListAdapter.class.getSimpleName();
    private List<AttendanceRecord> menuCategoryList;
    private Context context;
    private int rowLayout;

    public AttendanceListAdapter(List<AttendanceRecord> menuCategoryList, int rowLayout, Context context) {
        this.menuCategoryList = menuCategoryList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        try{
            if (menuCategoryList.get(position).getType() != null) {
                if (menuCategoryList.get(position).getType().getAttandanceTypeId() == 1) {
                    ((CustomViewHolder)holder).txtAttendanceType.getResources().getColor(R.color.teal_700);
                } else {
                    ((CustomViewHolder)holder).txtAttendanceType.getResources().getColor(R.color.error_color);
                }
            }
            ((CustomViewHolder)holder).txtClockIn.setText(AppUtils.convertMongoDateWithS(menuCategoryList.get(position).getAttandanceTime()));
            ((CustomViewHolder)holder).txtClockOut.setText(AppUtils.convertMongoDateWithS(menuCategoryList.get(position).getAttandanceCheckOut()));
            ((CustomViewHolder)holder).txtAttendanceType.setText(menuCategoryList.get(position).getType().getAttandanceTypeName());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView txtClockIn, txtClockOut, txtAttendanceType;
        public CustomViewHolder(View view) {
            super(view);
            txtClockIn = view.findViewById(R.id.txt_clock_in_time);
            txtClockOut = view.findViewById(R.id.txt_clock_out_time);
            txtAttendanceType = view.findViewById(R.id.txt_attendance_type);

            view.setTag(view);
        }
    }

    @Override
    public int getItemCount()  {
        return menuCategoryList == null ? 0 : menuCategoryList.size();
    }
}