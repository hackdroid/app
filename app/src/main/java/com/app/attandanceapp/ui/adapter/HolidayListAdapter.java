package com.app.attandanceapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.attandanceapp.R;
import com.app.attandanceapp.ui.listener.HolidayListener;
import com.app.attandanceapp.ui.model.HolidayResponse;

import java.util.List;

public class HolidayListAdapter extends RecyclerView.Adapter<HolidayListAdapter.CustomViewHolder> {
    private static final String TAG = HolidayListAdapter.class.getSimpleName();
    private List<HolidayResponse> menuCategoryList;
    private Context context;
    private int rowLayout;
    HolidayListener itemClickListener;

    public HolidayListAdapter(List<HolidayResponse> menuCategoryList, int rowLayout, Context context) {
        this.menuCategoryList = menuCategoryList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        /*try{
            String tableStatus = listOfPosts.get(position).getStatus();
            ((CustomViewHolder) holder).txtTableNumber.setText("T.No-" + listOfPosts.get(position).getTableNo());
            if (tableStatus.equals("0")){
                ((CustomViewHolder) holder).txtTableStatus.setText("Available");
        //        ((CustomViewHolder) holder).rlTable.setBackgroundColor(Color.parseColor("#006400"));
                ((CustomViewHolder) holder).rlTable.setBackgroundResource(R.drawable.circle);
            }else {
                ((CustomViewHolder) holder).txtTableStatus.setText("Booked");
        //        ((CustomViewHolder) holder).rlTable.setBackgroundColor(Color.parseColor("#6B767E"));
                ((CustomViewHolder) holder).rlTable.setBackgroundResource(R.drawable.circle_track_highlighted);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
*/
        try{
            String strStartDate = menuCategoryList.get(position).getCalendarDate();
            String[] strDateArray = strStartDate.split("-");
            String startDate = strDateArray[2];
            holder.txtTableNumber.setText(startDate);
            if (menuCategoryList.get(position).getCalendarIsHoliday()) {
            //    holder.txtTableNumber.getResources().getColor(R.color.teal_700);
                holder.rlTable.setBackground(ContextCompat.getDrawable(context, R.drawable.blue_circle));
             //   holder.rlTable
            } else {
            //    holder.txtTableNumber.setBackgroundColor(Color.parseColor("#000000"));
                holder.rlTable.setBackground(ContextCompat.getDrawable(context, R.drawable.gray_circle));
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        TextView txtTableNumber;
        RelativeLayout rlTable;

        public CustomViewHolder(View view) {
            super(view);
            txtTableNumber = view.findViewById(R.id.table_number);
            rlTable = view.findViewById(R.id.rl_table);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) itemClickListener.onHolidayClickListener(view, getAdapterPosition());
        }
    }

    public void setClickListener(HolidayListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount()  {
        return menuCategoryList == null ? 0 : menuCategoryList.size();
    }
}