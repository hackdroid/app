package com.app.attandanceapp.ui.listener;

import android.view.View;

public interface LeaveTypeListener {
    void onUserTypeClick(View view, int position);
}
