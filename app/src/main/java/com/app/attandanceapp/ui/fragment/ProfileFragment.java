package com.app.attandanceapp.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;
import com.app.attandanceapp.R;
import com.app.attandanceapp.api.ApiCallBack;
import com.app.attandanceapp.api.ApiManger;
import com.app.attandanceapp.api.ApiResponseListener;
import com.app.attandanceapp.helper.SessionManager;
import com.app.attandanceapp.ui.activity.ChangePasswordActivity;
import com.app.attandanceapp.ui.activity.HolidayActivity;
import com.app.attandanceapp.ui.activity.HomeActivity;
import com.app.attandanceapp.ui.activity.LoginActivity;
import com.app.attandanceapp.ui.model.FileUploadModel;
import com.app.attandanceapp.ui.model.LoginModel;
import com.app.attandanceapp.ui.model.SelfieUploadModel;
import com.app.attandanceapp.utils.AppUtils;
import com.app.attandanceapp.utils.Constant;
import com.app.attandanceapp.utils.FileUtil;
import com.app.attandanceapp.utils.ToastAlert;
import com.app.attandanceapp.utils.ToastUtils;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private NavigationView navigationView;
    Context context;
    SessionManager sessionManager;
    TextView txtMobile, txtName, txtEmail;
    AVLoadingIndicatorView indicatorView;
    private CircleImageView imgProfilePic;
    File file;
    ToastAlert alert;


    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ((HomeActivity) getActivity()).setTitle(getResources().getString(R.string.title_profile));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        sessionManager = new SessionManager(context);
        RelativeLayout relativeLayoutCamera = view.findViewById(R.id.rl_camera);
        imgProfilePic = view.findViewById(R.id.image_profile);
        navigationView = view.findViewById(R.id.nav_menu);
        txtName = view.findViewById(R.id.profile_name);
        txtEmail = view.findViewById(R.id.email);
        txtMobile = view.findViewById(R.id.mobile_number);
        indicatorView = view.findViewById(R.id.avi);
        indicatorView.smoothToHide();

        alert = new ToastAlert(getActivity());
        relativeLayoutCamera.setOnClickListener(this);

        indicatorView.smoothToShow();
        checkSession();
        setUpNavigationView();
    }

    @SuppressLint("NonConstantResourceId")
    private void setUpNavigationView() {
        this.navigationView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.setting:
                    Intent intent = new Intent(context, ChangePasswordActivity.class);
                    //        Intent intent = new Intent(context, SettingActivity.class);
                    startActivity(intent);
                    break;
                case R.id.holidays:
                    Intent order = new Intent(context, HolidayActivity.class);
                    startActivity(order);
                    break;
                case R.id.view_id_card:
                    getUserProfile();
                    break;
                case R.id.logout:
                    logoutMethod();
                    break;
            }
            return false;
        });
    }

    private void getUserProfile() {
        if (AppUtils.isNetworkAvailable(context)) {


        } else {
            alert.showNoInternet("No internet");
        }


    }

    private void checkSession() {
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<LoginModel> callBack = new ApiCallBack<>(new ApiResponseListener<LoginModel>() {
                @Override
                public void onApiSuccess(LoginModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            txtName.setText(response.getUser().getUserName());
                            txtMobile.setText("+91 " + response.getUser().getUserPhone());
                            txtEmail.setText(response.getUser().getUserEmail());
                            Glide.with(context)
                                    .load(response.getUser().getUserProfile())
                                    .error(R.drawable.monk)
                                    .placeholder(R.drawable.monk)
                                    .into(imgProfilePic);
                        } else {
                            checkSessionDialog();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    indicatorView.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                    indicatorView.smoothToHide();
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    indicatorView.smoothToHide();
                }
            }, Constant.CHECK_SESSION, context);

            apiManager.checkAuthApi(callBack);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    public void checkSessionDialog() {
        new IOSDialog.Builder(getActivity())
                .setTitle(context.getString(R.string.session_noti))
                .setMessage(context.getString(R.string.session_expired))
                .setPositiveButton(R.string.yes,
                        (dialog, which) -> {
                            logoutMethod();
                            dialog.dismiss();
                        })
                .setNegativeButton(R.string.no,
                        (dialog, which) -> {
                            dialog.dismiss();
                            getActivity().finish();
                        })
                .show();
    }

    private void logoutMethod() {
        sessionManager.setLogin(false);
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_camera:
                ImagePicker.Companion.with(this)
                        .cameraOnly()
                        .crop()
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri fileUri = data.getData();
            imgProfilePic.setImageURI(fileUri);
            try {
                file = FileUtil.from(context, fileUri);
                if (file == null) {
                    Toast.makeText(context, "Choose image first!", Toast.LENGTH_SHORT).show();
                } else {
                    indicatorView.smoothToShow();
                    uploadImage();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImage() {
        if (AppUtils.isNetworkAvailable(context)) {
            RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), requestImage);
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<FileUploadModel> callBack = new ApiCallBack<>(new ApiResponseListener<FileUploadModel>() {
                @Override
                public void onApiSuccess(FileUploadModel response, String apiName) {
                    try {
                        if (!response.getError()) {
                            postImageOnServer(response.getFilename());
                        } else {
                            Toast.makeText(context, response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    indicatorView.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    indicatorView.smoothToHide();
                }
            }, Constant.POST_URL_FILE_UPLOAD_API, context);
            apiManager.uploadFileApi(callBack, image);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }

    private void postImageOnServer(final String imageUrl) {
        if (AppUtils.isNetworkAvailable(context)) {
            ApiManger apiManager = new ApiManger(context, true);
            ApiCallBack<SelfieUploadModel> callBack = new ApiCallBack<>(new ApiResponseListener<SelfieUploadModel>() {
                @Override
                public void onApiSuccess(SelfieUploadModel response, String apiName) {
                    try {
                        Toast.makeText(context, response.getMsg(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    indicatorView.smoothToHide();
                }

                @Override
                public void onApiError(String responses, String apiName) {
                    indicatorView.smoothToHide();
                    ToastUtils.showToastShort(context, "The information you have entered is incorrect. Please try again.");
                }

                @Override
                public void onApiFailure(String failureMessage, String apiName) {
                    indicatorView.smoothToHide();
                }
            }, Constant.LOGIN_API, context);

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("profile", imageUrl);
            apiManager.setProfilePicApi(callBack, jsonObject);
        } else {
            ToastUtils.showToastShort(context, Constant.CHECK_CONNECTION);
        }
    }
}