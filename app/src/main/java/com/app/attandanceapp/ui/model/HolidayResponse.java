package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HolidayResponse {
    @SerializedName("calendar_id")
    @Expose
    private int calendarId;
    @SerializedName("calendar_date")
    @Expose
    private String calendarDate;
    @SerializedName("calendar_month")
    @Expose
    private int calendarMonth;
    @SerializedName("calendar_is_holiday")
    @Expose
    private Boolean calendarIsHoliday;
    @SerializedName("calendar_created_by")
    @Expose
    private String calendarCreatedBy;
    @SerializedName("calendar_created_on")
    @Expose
    private String calendarCreatedOn;
    @SerializedName("calendar_label")
    @Expose
    private String calendarLabel;
    @SerializedName("calendar_day")
    @Expose
    private String calendarDay;

    public int getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(int calendarId) {
        this.calendarId = calendarId;
    }

    public String getCalendarDate() {
        return calendarDate;
    }

    public void setCalendarDate(String calendarDate) {
        this.calendarDate = calendarDate;
    }

    public int getCalendarMonth() {
        return calendarMonth;
    }

    public void setCalendarMonth(int calendarMonth) {
        this.calendarMonth = calendarMonth;
    }

    public Boolean getCalendarIsHoliday() {
        return calendarIsHoliday;
    }

    public void setCalendarIsHoliday(Boolean calendarIsHoliday) {
        this.calendarIsHoliday = calendarIsHoliday;
    }

    public String getCalendarCreatedBy() {
        return calendarCreatedBy;
    }

    public void setCalendarCreatedBy(String calendarCreatedBy) {
        this.calendarCreatedBy = calendarCreatedBy;
    }

    public String getCalendarCreatedOn() {
        return calendarCreatedOn;
    }

    public void setCalendarCreatedOn(String calendarCreatedOn) {
        this.calendarCreatedOn = calendarCreatedOn;
    }

    public String getCalendarLabel() {
        return calendarLabel;
    }

    public void setCalendarLabel(String calendarLabel) {
        this.calendarLabel = calendarLabel;
    }

    public String getCalendarDay() {
        return calendarDay;
    }

    public void setCalendarDay(String calendarDay) {
        this.calendarDay = calendarDay;
    }
}
