package com.app.attandanceapp.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InboxRecordModel {
    @SerializedName("leave_application_id")
    @Expose
    private Integer leaveApplicationId;
    @SerializedName("leave_application_date")
    @Expose
    private String leaveApplicationDate;
    @SerializedName("leave_application_time")
    @Expose
    private String leaveApplicationTime;
    @SerializedName("leave_application_user")
    @Expose
    private Integer leaveApplicationUser;
    @SerializedName("leave_application_status")
    @Expose
    private Boolean leaveApplicationStatus;
    @SerializedName("leave_application_checked_by")
    @Expose
    private Integer leaveApplicationCheckedBy;
    @SerializedName("leave_application_checked_on")
    @Expose
    private String leaveApplicationCheckedOn;
    @SerializedName("leave_application_remarks")
    @Expose
    private String leaveApplicationRemarks;
    @SerializedName("leave_application_type")
    @Expose
    private Integer leaveApplicationType;
    @SerializedName("leave_application_start_date")
    @Expose
    private String leaveApplicationStartDate;
    @SerializedName("leave_application_end_date")
    @Expose
    private String leaveApplicationEndDate;
    @SerializedName("leave_type")
    @Expose
    private LeaveType leaveType;
    @SerializedName("checked_by")
    @Expose
    private UserModel checkedBy;

    public Integer getLeaveApplicationId() {
        return leaveApplicationId;
    }

    public void setLeaveApplicationId(Integer leaveApplicationId) {
        this.leaveApplicationId = leaveApplicationId;
    }

    public String getLeaveApplicationDate() {
        return leaveApplicationDate;
    }

    public void setLeaveApplicationDate(String leaveApplicationDate) {
        this.leaveApplicationDate = leaveApplicationDate;
    }

    public String getLeaveApplicationTime() {
        return leaveApplicationTime;
    }

    public void setLeaveApplicationTime(String leaveApplicationTime) {
        this.leaveApplicationTime = leaveApplicationTime;
    }

    public Integer getLeaveApplicationUser() {
        return leaveApplicationUser;
    }

    public void setLeaveApplicationUser(Integer leaveApplicationUser) {
        this.leaveApplicationUser = leaveApplicationUser;
    }

    public Boolean getLeaveApplicationStatus() {
        return leaveApplicationStatus;
    }

    public void setLeaveApplicationStatus(Boolean leaveApplicationStatus) {
        this.leaveApplicationStatus = leaveApplicationStatus;
    }

    public Integer getLeaveApplicationCheckedBy() {
        return leaveApplicationCheckedBy;
    }

    public void setLeaveApplicationCheckedBy(Integer leaveApplicationCheckedBy) {
        this.leaveApplicationCheckedBy = leaveApplicationCheckedBy;
    }

    public String getLeaveApplicationCheckedOn() {
        return leaveApplicationCheckedOn;
    }

    public void setLeaveApplicationCheckedOn(String leaveApplicationCheckedOn) {
        this.leaveApplicationCheckedOn = leaveApplicationCheckedOn;
    }

    public String getLeaveApplicationRemarks() {
        return leaveApplicationRemarks;
    }

    public void setLeaveApplicationRemarks(String leaveApplicationRemarks) {
        this.leaveApplicationRemarks = leaveApplicationRemarks;
    }

    public Integer getLeaveApplicationType() {
        return leaveApplicationType;
    }

    public void setLeaveApplicationType(Integer leaveApplicationType) {
        this.leaveApplicationType = leaveApplicationType;
    }

    public String getLeaveApplicationStartDate() {
        return leaveApplicationStartDate;
    }

    public void setLeaveApplicationStartDate(String leaveApplicationStartDate) {
        this.leaveApplicationStartDate = leaveApplicationStartDate;
    }

    public String getLeaveApplicationEndDate() {
        return leaveApplicationEndDate;
    }

    public void setLeaveApplicationEndDate(String leaveApplicationEndDate) {
        this.leaveApplicationEndDate = leaveApplicationEndDate;
    }

    public LeaveType getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(LeaveType leaveType) {
        this.leaveType = leaveType;
    }

    public UserModel getCheckedBy() {
        return checkedBy;
    }

    public void setCheckedBy(UserModel checkedBy) {
        this.checkedBy = checkedBy;
    }
}
