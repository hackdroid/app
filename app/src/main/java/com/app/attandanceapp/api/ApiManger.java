package com.app.attandanceapp.api;

import android.content.Context;

import com.google.gson.JsonObject;
import com.app.attandanceapp.ui.model.AttendanceListModel;
import com.app.attandanceapp.ui.model.CheckOutModel;
import com.app.attandanceapp.ui.model.EncryptModel;
import com.app.attandanceapp.ui.model.FileUploadModel;
import com.app.attandanceapp.ui.model.ForgotPasswordModel;
import com.app.attandanceapp.ui.model.HolidayModel;
import com.app.attandanceapp.ui.model.InboxModel;
import com.app.attandanceapp.ui.model.LeaveTypeModel;
import com.app.attandanceapp.ui.model.LoginModel;
import com.app.attandanceapp.ui.model.MarkAttendanceModel;
import com.app.attandanceapp.ui.model.OTPVerifyModel;
import com.app.attandanceapp.ui.model.ResetPasswordModel;
import com.app.attandanceapp.ui.model.SelfieUploadModel;
import com.app.attandanceapp.ui.model.TimeShiftModel;

import okhttp3.MultipartBody;

public class ApiManger extends ApiClient {
    private static ApiManger apiManager;
    private Context mContext;

    public ApiManger(Context context, Boolean apiIS) {
        super(context, apiIS);
        mContext = context;
    }

    public static ApiManger getInstance(Context context, Boolean api) {
        if (apiManager == null) {
            apiManager = new ApiManger(context, api);
        }
        return apiManager;
    }

    public void resetPasswordApi(ApiCallBack<ResetPasswordModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).resetPassword(jsonObject).enqueue(callBack);
    }

    public void otpVerifyApi(ApiCallBack<OTPVerifyModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).otpVerify(jsonObject).enqueue(callBack);
    }

    public void forgotPasswordApi(ApiCallBack<ForgotPasswordModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).forgotPassword(jsonObject).enqueue(callBack);
    }

    public void loginApi(ApiCallBack<LoginModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).loginMethod(jsonObject).enqueue(callBack);
    }

    public void checkAuthApi(ApiCallBack<LoginModel> callBack) {
        ApiClient.current(mContext, true).getAccessToken().enqueue(callBack);
    }

    public void latLongEncryptApi(ApiCallBack<EncryptModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).latLongEncryptMethod(jsonObject).enqueue(callBack);
    }

    public void timeShiftApi(ApiCallBack<TimeShiftModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).timeShiftMethod(jsonObject).enqueue(callBack);
    }

    public void markAttendanceApi(ApiCallBack<MarkAttendanceModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).markAttendanceMethod(jsonObject).enqueue(callBack);
    }

    public void checkOutApi(ApiCallBack<CheckOutModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).checkOutMethod(jsonObject).enqueue(callBack);
    }

    public void uploadFileApi(ApiCallBack<FileUploadModel> callBack, MultipartBody.Part image) {
        ApiClient.current(mContext, true).uploadFiles(image).enqueue(callBack);
    }

    public void setProfilePicApi(ApiCallBack<SelfieUploadModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).selfiUploadMethod(jsonObject).enqueue(callBack);
    }

    public void getLeaveHistoryApi(ApiCallBack<InboxModel> callBack) {
        ApiClient.current(mContext, true).getLeaveHistory().enqueue(callBack);
    }

    public void getLeaveTypeApi(ApiCallBack<LeaveTypeModel> callBack) {
        ApiClient.current(mContext, false).getLeaveType().enqueue(callBack);
    }

    public void createNewLeaveApi(ApiCallBack<InboxModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).createNewLeave(jsonObject).enqueue(callBack);
    }

    public void getAttendanceListApi(ApiCallBack<AttendanceListModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).getAttendance(jsonObject).enqueue(callBack);
    }

    public void getHolidayListApi(ApiCallBack<HolidayModel> callBack, int noOfMonth) {
        ApiClient.current(mContext, false).getHolidayList(noOfMonth).enqueue(callBack);
    }

    /*



    public void getSubCategoryApi(ApiCallBack<SubCategoryResponse> callBack, String id) {
        ApiClient.current(mContext, true).getSubCategoryList(id).enqueue(callBack);
    }

    public void getAccessTypeApi(ApiCallBack<AccessTypeResponse> callBack, String id) {
        ApiClient.current(mContext, true).getAccessType(id).enqueue(callBack);
    }



    public void submitFormSecondPart(ApiCallBack<FormPartTwoResponse> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).formSecondPart(jsonObject).enqueue(callBack);
    }

    public void forgotPasswordApi(ApiCallBack<ForgotPassResponse> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).forgotPassword(jsonObject).enqueue(callBack);
    }

    public void changePasswordApi(ApiCallBack<ChangePasswordModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).changePassword(jsonObject).enqueue(callBack);
    }

    public void getLeadApi(ApiCallBack<LeadResponse> callBack, int limit, String type) {
        ApiClient.current(mContext, true).getLeadList(limit, type).enqueue(callBack);
    }

    public void getLeadDetailApi(ApiCallBack<LeadDetailResponse> callBack, String id) {
        ApiClient.current(mContext, true).getLeadDetail(id).enqueue(callBack);
    }

    public void validateApi(ApiCallBack<CheckVehicleNumber> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).checkVehicleNumberApi(jsonObject).enqueue(callBack);
    }

    public void helpApi(ApiCallBack<SupportModel> callBack) {
        ApiClient.current(mContext, false).helpSupport().enqueue(callBack);
    }

    public void loginWithMobileApi(ApiCallBack<LoginWithMobileModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).loginWithMobile(jsonObject).enqueue(callBack);
    }

    public void loginWithOtpApi(ApiCallBack<OtpResponse> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).loginWithOtp(jsonObject).enqueue(callBack);
    }

    public void salesOtpVerifyApi(ApiCallBack<SalesOtpVerifyModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).salesOtpVerify(jsonObject).enqueue(callBack);
    }

    public void generalProfileApi(ApiCallBack<GeneralProfileModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).generalProfileMethod(jsonObject).enqueue(callBack);
    }

    public void userUploadSelfieApi(ApiCallBack<SelfieUploadModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, true).userSelfieUploadMethod(jsonObject).enqueue(callBack);
    }

    public void resendOtpApi(ApiCallBack<ResendOTPModel> callBack, JsonObject jsonObject) {
        ApiClient.current(mContext, false).resendOTPMethod(jsonObject).enqueue(callBack);
    }

    public void getUserTypeApi(ApiCallBack<UserTypeResponse> callBack) {
        ApiClient.current(mContext, false).getUserType().enqueue(callBack);
    }

    public void getLeadCountApi(ApiCallBack<LeadCountModel> callBack) {
        ApiClient.current(mContext, true).getLeadCount().enqueue(callBack);
    }

    public void getStateListApi(ApiCallBack<StateModel> callBack) {
        ApiClient.current(mContext, false).getStateList().enqueue(callBack);
    }

    public void getCityListApi(ApiCallBack<CityModel> callBack, String index) {
        ApiClient.current(mContext, false).getCityList(index).enqueue(callBack);
    }

    public void checkLeadValidateApi(ApiCallBack<ValidateLeadModel> callBack, JsonObject index) {
        ApiClient.current(mContext, true).validateLead(index).enqueue(callBack);
    }

    public void getBrandListApi(ApiCallBack<BrandResponse> callBack) {
        ApiClient.current(mContext, true).getBrandList().enqueue(callBack);
    }

    public void getModelListApi(ApiCallBack<ModelResponse> callBack, String brandId) {
        ApiClient.current(mContext, true).getModelList(brandId).enqueue(callBack);
    }

    public void addDriverApi(ApiCallBack<AddDriverResponse> callBack, JsonObject index) {
        ApiClient.current(mContext, true).addDriver(index).enqueue(callBack);
    }

    public void getDriverListApi(ApiCallBack<DriverListModel> callBack) {
        ApiClient.current(mContext, true).getDriverList().enqueue(callBack);
    }

    public void getVehicleListApi(ApiCallBack<LeadResponse> callBack) {
        ApiClient.current(mContext, true).getVehicleList().enqueue(callBack);
    }

    public void getRewardListApi(ApiCallBack<RewardResponse> callBack) {
        ApiClient.current(mContext, true).getRewardList().enqueue(callBack);
    }

    public void getIfscListApi(ApiCallBack<IfscResponse> callBack, JsonObject index) {
        ApiClient.current(mContext, false).getIfscCode(index).enqueue(callBack);
    }

    public void getAddAccountApi(ApiCallBack<AddAccountResponse> callBack, JsonObject index) {
        ApiClient.current(mContext, true).getAddAccount(index).enqueue(callBack);
    }

    public void getBankAccountListApi(ApiCallBack<BankAccountListResponse> callBack) {
        ApiClient.current(mContext, true).getBankAccountList().enqueue(callBack);
    }

    public void getBankAccountActiveApi(ApiCallBack<BankAccountListResponse> callBack, JsonObject index) {
        ApiClient.current(mContext, true).getAccountActive(index).enqueue(callBack);
    }

    public void getMapDriverApi(ApiCallBack<DriverListModel> callBack, JsonObject index) {
        ApiClient.current(mContext, true).getMapDriver(index).enqueue(callBack);
    }

    public void getUnMapDriverApi(ApiCallBack<DriverListModel> callBack, JsonObject index) {
        ApiClient.current(mContext, true).getUnMapDriver(index).enqueue(callBack);
    }*/
}
