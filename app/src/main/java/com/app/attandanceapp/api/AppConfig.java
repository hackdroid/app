package com.app.attandanceapp.api;

public class AppConfig {
    public static final String POST_URL_LOGIN_API = "auth/login";
    public static final String POST_URL_RESET_PASSWORD_API = "auth/password/create";
    public static final String POST_URL_FORGOT_PASSWORD_API = "auth/reset";
    public static final String POST_URL_OTP_VERIFY_API = "auth/verify";
    public static final String GET_URL_ACCESS_TOKEN = "auth/accesstoken";
    public static final String POST_URL_UPLOAD_FILE_API = "file/upload";
    public static final String PUT_URL_FILE_UPLOAD = "auth/update/profile";
    public static final String POST_URL_LAT_LONG_ENCRYPT = "util/encrypt/latlong";
    public static final String POST_URL_TIME_SHIFT = "auth/get/timeshift";
    public static final String POST_URL_MARK_ATTENDANCE = "attandanceapp/mark/attandanceapp";
    public static final String POST_URL_CHECKOUT = "attandanceapp/mark/checkout/";
    public static final String GET_URL_LEAVE_HISTORY = "leave/history";
    public static final String GET_URL_LEAVE_TYPE = "leave";
    public static final String POST_URL_CREATE_LEAVE = "leave/new";
    public static final String POST_URL_ATTENDANCE_LIST = "attandanceapp/list";
    public static final String GET_URL_HOLIDAY_LIST = "util/holidays";
}
