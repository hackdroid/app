package com.app.attandanceapp.api;

import com.google.gson.JsonObject;
import com.app.attandanceapp.ui.model.AttendanceListModel;
import com.app.attandanceapp.ui.model.CheckOutModel;
import com.app.attandanceapp.ui.model.EncryptModel;
import com.app.attandanceapp.ui.model.FileUploadModel;
import com.app.attandanceapp.ui.model.ForgotPasswordModel;
import com.app.attandanceapp.ui.model.HolidayModel;
import com.app.attandanceapp.ui.model.InboxModel;
import com.app.attandanceapp.ui.model.LeaveTypeModel;
import com.app.attandanceapp.ui.model.LoginModel;
import com.app.attandanceapp.ui.model.MarkAttendanceModel;
import com.app.attandanceapp.ui.model.OTPVerifyModel;
import com.app.attandanceapp.ui.model.ResetPasswordModel;
import com.app.attandanceapp.ui.model.SelfieUploadModel;
import com.app.attandanceapp.ui.model.TimeShiftModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {
    @POST(AppConfig.POST_URL_RESET_PASSWORD_API)
    Call<ResetPasswordModel> resetPassword(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_OTP_VERIFY_API)
    Call<OTPVerifyModel> otpVerify(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_FORGOT_PASSWORD_API)
    Call<ForgotPasswordModel> forgotPassword(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_LOGIN_API)
    Call<LoginModel> loginMethod(@Body JsonObject jsonObject);

    @GET(AppConfig.GET_URL_ACCESS_TOKEN)
    Call<LoginModel> getAccessToken();

    @POST(AppConfig.POST_URL_LAT_LONG_ENCRYPT)
    Call<EncryptModel> latLongEncryptMethod(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_TIME_SHIFT)
    Call<TimeShiftModel> timeShiftMethod(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_MARK_ATTENDANCE)
    Call<MarkAttendanceModel> markAttendanceMethod(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_CHECKOUT)
    Call<CheckOutModel> checkOutMethod(@Body JsonObject jsonObject);

    @Multipart
    @POST(AppConfig.POST_URL_UPLOAD_FILE_API)
    Call<FileUploadModel> uploadFiles(@Part MultipartBody.Part image);

    @PUT(AppConfig.PUT_URL_FILE_UPLOAD)
    Call<SelfieUploadModel> selfiUploadMethod(@Body JsonObject jsonObject);

    @GET(AppConfig.GET_URL_LEAVE_HISTORY)
    Call<InboxModel> getLeaveHistory();

    @GET(AppConfig.GET_URL_LEAVE_TYPE)
    Call<LeaveTypeModel> getLeaveType();

    @POST(AppConfig.POST_URL_CREATE_LEAVE)
    Call<InboxModel> createNewLeave(@Body JsonObject jsonObject);

    @POST(AppConfig.POST_URL_ATTENDANCE_LIST)
    Call<AttendanceListModel> getAttendance(@Body JsonObject jsonObject);

    @GET(AppConfig.GET_URL_HOLIDAY_LIST)
    Call<HolidayModel> getHolidayList(@Query("month") int noOfMonth);

    /*

    @POST(AppConfig.POST_URL_ACCESS_TOKEN)
    Call<CheckAuthenticationModel> checkAuth();
    */
}
