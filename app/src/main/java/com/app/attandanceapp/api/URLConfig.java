package com.app.attandanceapp.api;

public class URLConfig {
    public static final String URL_TERMS_CONDITIONS = "https://sekhulogistics.in/terms-conditions/";
    public static final String URL_PRIVACY_POLICY = "https://sekhulogistics.in/privacy-policy-2/";
    public static final String URL_ABOUT_US = "https://sekhulogistics.in/about-us/";
    public static final String URL_HowToUse = "http://app.sekhulogistics.in/HowToUse";

    public static final String HEADING_TERMS = "Terms & Conditions";
    public static final String HEADING_PRIVACY = "Privacy Policy";
    public static final String HEADING_About = "About Us";
    public static final String HEADING_HowToUse = "How to use";
}
