package com.app.attandanceapp.app;

import android.app.Application;
import android.util.Log;

import com.app.attandanceapp.utils.AppSignatureHelper;

import java.util.ArrayList;

public class MyApplication extends Application {

    public void onCreate() {
        super.onCreate();
        //    Stetho.initializeWithDefaults(this);

        ArrayList<String> signatures = AppSignatureHelper.getAppSignatures(this);
        for (String s : signatures) {
            Log.d("SmsRetrieverDebug", "App signature: \"" + s + "\"");
        }
    }
}
